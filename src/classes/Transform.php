<?php

namespace App\Classes;

class Transform
{
    /**
     * @var string
     */
    public $firstString;
    public $secString;
    public $thirdString;

    /**
     * Transform constructor.
     * @param $firstString
     * @param $secString
     * @param $thirdString
     */
    public function __construct($firstString, $secString, $thirdString)
    {
        $this->firstString = htmlspecialchars($firstString);
        $this->secString   = htmlspecialchars($secString);
        $this->thirdString = htmlspecialchars($thirdString);

        $this->saveToFile();

    }

    private function saveToFile() {
            $file = 'file';
            file_put_contents($file, $this->firstString.$this->secString.$this->thirdString);
    }

    /**
     * @param $string
     * @return string
     */
    private function reverseString($string)
    {
        return strrev($string);
    }

    /**
     * @param $string
     * @return string
     */
    private function capitaliseString($string)
    {
        return strtoupper($string);
    }

    /**
     * @param $string
     * @return string
     */
    private function reverseAndCapitaliseString($string)
    {
        return strrev(strtoupper($string));
    }

    /**
     * @param $firstString
     * @param $secString
     * @param $thirdString
     * @return string
     */
    public function concatenatedStrings($firstString, $secString, $thirdString) {
        return  $this->reverseString($firstString) .
                $this->capitaliseString($secString) .
                $this->reverseAndCapitaliseString($thirdString);
    }

    /**
     * @param $firstString
     * @param $secString
     * @param $thirdString
     * @return string
     */
    public function hashed($firstString, $secString, $thirdString) {
        return md5($this->concatenatedStrings($firstString, $secString, $thirdString));
    }

}
