<!DOCTYPE html>
<html lang="sk">

<?php

    use App\Classes\Transform;

    require_once __DIR__ . '/../vendor/autoload.php';

    include __DIR__ . '/../src/templates/header.phtml';

?>

<body>
    <div class="container pt-5">

        <form action="index.php" method="post" class="d-flex flex-column pb-5 w-25">
            <label title="first"> First string
                <input type="text" class="form-control" name="first" required>
            </label>
            <label title="second"> Second string
                <input type="text" class="form-control" name="second" required>
            </label>
            <label title="second"> Third string
                <input type="text" class="form-control" name="third" required>
            </label>
            <button class="w-50 btn btn-primary" name="submit" type="submit">Submit</button>
        </form>


    <?php

        if (isset($_POST['submit'])) {

            if (!empty(trim($_POST['first'])) && !empty(trim($_POST['second'])) && !empty(trim($_POST['third']))) {

                $firstString = $_POST[ 'first' ];
                $secString   = $_POST[ 'second' ];
                $thirdString = $_POST[ 'third' ];

                $strings = new Transform($firstString, $secString, $thirdString);

                echo "<p><strong>Povodne stringy: </strong>{$strings->firstString} {$strings->secString} {$strings->thirdString} </p>";
                echo "<p><strong>Spojene stringy: </strong>{$strings->concatenatedStrings($firstString,$secString,$thirdString)}</p>";
                echo "<p><strong>Hashovany spojene stringy: </strong>{$strings->hashed($firstString,$secString,$thirdString)} </p>";

            } else {
                echo '<p class="text-danger">Dopln vsetky inputy</p>';
            }
        }

    ?>

        <a href="index.php" class="text-muted">reset</a>
    </div>
</body>
</html>
